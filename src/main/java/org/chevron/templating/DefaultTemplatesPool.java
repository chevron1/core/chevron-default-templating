package org.chevron.templating;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.chevron.model.Format;
import org.chevron.model.Template;
import org.chevron.model.TemplatesPool;

import java.util.*;

public class DefaultTemplatesPool implements TemplatesPool {

    private List<TemplateLoader> storeList = new ArrayList<>();
    private Map<String,Template> templatesCache = new HashMap<>();
    private boolean caching = true;

    private static final Logger LOGGER = LogManager.getLogger(DefaultTemplatesPool.class);

    private DefaultTemplatesPool(){}

    public DefaultTemplatesPool(TemplateLoader... store){
        this.storeList.addAll(Arrays.asList(
                store));
    }

    @Override
    public Optional<Template> getTemplate(String name, Format format, List<String> qualifiers) {
        if(name==null||name.isEmpty())
            throw new IllegalArgumentException("name must not be null nor empty");
        if(format==null)
            throw new IllegalArgumentException("format must not be null");
        String templateQualifiedName = getQualifiedName(name,qualifiers);
        LOGGER.info("Searching for template [{}]",templateQualifiedName);
        if(caching) {
            LOGGER.debug("Checking if template is cached");
            if (templatesCache.containsKey(templateQualifiedName)) {
                LOGGER.debug("Template found in cache");
                return Optional.of(templatesCache.get(templateQualifiedName));
            }else LOGGER.debug("Template not found in cache");
        }
        for(TemplateLoader loader: storeList){
            LOGGER.debug("Searching for template in loader {}",loader.toString());
            Optional<Template> templateOptional = loader.load(name, format, qualifiers);
            if(templateOptional.isPresent()) {
                LOGGER.debug("Template found");
                if(caching) {
                    LOGGER.debug("Caching template");
                    templatesCache.put(templateQualifiedName, templateOptional.get());
                }
                return templateOptional;
            }
        }
        LOGGER.debug("Template not found in all available Loaders");
        return Optional.empty();
    }


    private String getQualifiedName(String name, List<String> qualifiers){
        StringBuilder fqn = new StringBuilder(name);
        for(String qualifier: qualifiers){
            fqn.append(".");
            fqn.append(qualifier);
        }
        return fqn.toString();
    }

    public static Builder builder(){

        return new Builder();

    }

    public static class Builder {

        private DefaultTemplatesPool defaultTemplatesPool = new DefaultTemplatesPool();

        public Builder enlist(TemplateLoader loader){
            if(loader==null)
                throw new IllegalArgumentException("loader reference must not be null");
            loader.setPool(defaultTemplatesPool);
            defaultTemplatesPool.storeList.add(
                    loader);
            return this;
        }

        public Builder disableCaching(){
            this.defaultTemplatesPool.caching = false;
            return this;
        }

        public Builder enableCaching(){
            this.defaultTemplatesPool.caching = true;
            return this;
        }

        public DefaultTemplatesPool build(){

            return this.defaultTemplatesPool;

        }

    }


}
