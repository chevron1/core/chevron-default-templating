package org.chevron.templating;

import org.chevron.model.Format;
import org.chevron.model.Template;
import org.chevron.model.TemplatesPool;

import java.util.List;
import java.util.Optional;

public interface TemplateLoader {

    void setPool(TemplatesPool pool);
    Optional<Template> load(String name, Format format, List<String> qualifiers);

}
