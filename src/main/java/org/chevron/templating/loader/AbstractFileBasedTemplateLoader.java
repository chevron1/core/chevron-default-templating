package org.chevron.templating.loader;

import org.apache.commons.io.IOUtils;
import org.chevron.model.ChevronException;
import org.chevron.model.Format;
import org.chevron.model.Template;
import org.chevron.templating.TemplateLoader;
import org.yaml.snakeyaml.Yaml;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public abstract class AbstractFileBasedTemplateLoader extends AbstractTemplateLoader {

    protected String getQualifiedName(String name, Format format,  List<String> qualifiers){
        StringBuilder fqn = new StringBuilder(name);
        for(String qualifier: qualifiers){
            fqn.append('.');
            fqn.append(qualifier);
        }
        fqn.append('.');
        fqn.append(format.getFileExtension());
        return fqn.toString();
    }

    protected String getMetadataFileName(String templateQualifiedName){
        return templateQualifiedName+".yaml";
    }

    @Override
    public Optional<Template> load(String name, Format format, List<String> qualifiers) {
        String qualifiedName = getQualifiedName(name, format, qualifiers);
        Optional<InputStream> sourceInputStream = getSourceInputStream(qualifiedName);
        if(sourceInputStream.isPresent()){
            Optional<InputStream> metadataInputStream = getMetadataInputStream(qualifiedName);
            if(metadataInputStream.isPresent()){
                TemplateMetadata metadata = this.parseTemplateMetadata(qualifiedName,metadataInputStream
                        .get());
                String content = "";
                try {
                    content = IOUtils.toString(sourceInputStream.get(), "utf-8");
                }catch (IOException ex){
                    throw new ChevronException("error reading template content from provided stream",
                            ex);
                }
                return Optional.of(new TemplateImpl(getPool(),name, format, content,metadata,
                        qualifiers));
            }else throw new ChevronException(String.format("Template metadata file NOT found but template file exists [%s]",
                    qualifiedName));
        }
        return Optional.empty();
    }

    private TemplateMetadata parseTemplateMetadata(String name, InputStream inputStream){
        Yaml yaml = new Yaml();
        Map<String, String> properties = null;
        try {
            properties = yaml.load(inputStream);
        }catch (Exception ex){
            throw new ChevronException(String.format("error parsing template metadata file [%s]",name),
                    ex);
        }
        String language = properties.getOrDefault("language",null);
        String schema = properties.getOrDefault("schema",null);
        if(language==null)
            throw new ChevronException(String.format("template language not defined in metadata file [%s]",name));
        if(schema==null)
            throw new ChevronException(String.format("schema not defined in template metadata file [%s]",name));
        return new TemplateMetadata(language, schema);
    }

    protected abstract Optional<InputStream> getSourceInputStream(String qualifiedName);
    protected abstract Optional<InputStream> getMetadataInputStream(String qualifiedName);

}
