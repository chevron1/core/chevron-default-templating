package org.chevron.templating.loader;

import org.chevron.model.TemplatesPool;
import org.chevron.templating.TemplateLoader;

public abstract class AbstractTemplateLoader implements TemplateLoader {

    private TemplatesPool pool;

    public void setPool(TemplatesPool pool){
        if(pool==null)
            throw new IllegalArgumentException("pool must not be null");
        this.pool = pool;
    }

    protected TemplatesPool getPool(){

        return this.pool;

    }

}
