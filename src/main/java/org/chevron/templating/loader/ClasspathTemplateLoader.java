package org.chevron.templating.loader;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.InputStream;
import java.util.Optional;

public class ClasspathTemplateLoader extends AbstractFileBasedTemplateLoader {

    private String path;
    private ClassLoader classLoader;

    private static final Logger LOGGER = LogManager.getLogger(ClasspathTemplateLoader.class);

    public ClasspathTemplateLoader(String path){
        this(path, ClasspathTemplateLoader.class.
                getClassLoader());
    }

    public ClasspathTemplateLoader(String path, ClassLoader classLoader){
        if(path==null||path.isEmpty())
            throw new IllegalArgumentException("path must not be null nor empty");
        if(classLoader==null)
            throw new IllegalArgumentException("classLoader must not null");
        this.path = path;
        this.classLoader = classLoader;
    }


    @Override
    protected Optional<InputStream> getSourceInputStream(String qualifiedName) {
        String sourcePath = path+"/"+qualifiedName;
        LOGGER.debug("Template resource: {}",sourcePath);
        return Optional.ofNullable(classLoader.getResourceAsStream(sourcePath));
    }

    @Override
    protected Optional<InputStream> getMetadataInputStream(String qualifiedName) {
        String resource = path+"/"+ getMetadataFileName(qualifiedName);
        LOGGER.debug("Template metadata resource: {}",resource);
        return Optional.ofNullable(classLoader.getResourceAsStream(
                resource));
    }

    public String toString(){
        return String.format("%s[%s]",ClasspathTemplateLoader.class.getSimpleName(),
                path);
    }

}
