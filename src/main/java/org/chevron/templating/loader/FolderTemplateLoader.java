package org.chevron.templating.loader;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.chevron.model.ChevronException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

public class FolderTemplateLoader extends AbstractFileBasedTemplateLoader {

    private File file;

    private static final Logger LOGGER = LogManager.getLogger(
            FolderTemplateLoader.class);

    public FolderTemplateLoader(String path){
        file = new File(path);
        if(!file.exists()||!file.isDirectory())
            throw new IllegalArgumentException("path must point must point to an existing folder");
    }

    @Override
    protected Optional<InputStream> getSourceInputStream(String qualifiedName) {
        String path = file.getAbsolutePath()+File.separator+qualifiedName;
        LOGGER.debug("Template file: {}",path);
        File file = new File(path);
        if(!file.exists())
            return Optional.empty();
        try {
            return Optional.of(new FileInputStream(new File(path)));
        }catch (IOException ex){
            throw new ChevronException("error opening template file: "+path,
                    ex);
        }
    }

    @Override
    protected Optional<InputStream> getMetadataInputStream(String qualifiedName) {
        String path = file.getAbsolutePath()+File.separator+getMetadataFileName(qualifiedName);
        LOGGER.debug("Template metadata file: {}",path);
        File file = new File(path);
        if(!file.exists())
            return Optional.empty();
        try {
            return Optional.of(new FileInputStream(new File(path)));
        }catch (IOException ex){
            throw new ChevronException("error opening template Metadata file: "+path,
                    ex);
        }
    }

    public String toString(){
        return String.format("%s[%s]",ClasspathTemplateLoader.class.getSimpleName(), file.getAbsolutePath());
    }

}
