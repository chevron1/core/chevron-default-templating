package org.chevron.templating.loader;

import org.chevron.core.templating.AbstractTemplate;
import org.chevron.model.Format;
import org.chevron.model.TemplatesPool;
import org.chevron.model.schema.ObjectSchemaRef;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TemplateImpl extends AbstractTemplate  {

    private String name;
    private String language;
    private ObjectSchemaRef objectSchemaRef;
    private List<String> qualifiers = new ArrayList<>();
    private String rawSource;
    private Format format;

    TemplateImpl(TemplatesPool templatesPool, String name, Format format, String rawSource, TemplateMetadata metadata, List<String> qualifiers){
        super(templatesPool);
        this.name = name;
        this.format = format;
        this.language = metadata.getLanguage();
        this.objectSchemaRef = new ObjectSchemaRef(metadata.getSchema());
        this.rawSource = rawSource;
        this.qualifiers = qualifiers;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getLanguage() {
        return this.language;
    }

    @Override
    public ObjectSchemaRef getSchema() {
        return this.objectSchemaRef;
    }

    @Override
    public List<String> getQualifiers() {
        return Collections.unmodifiableList(
                qualifiers);
    }

    @Override
    protected String getRawSource() {
        return this.rawSource;
    }

    @Override
    public Format getFormat() {
        return format;
    }
}
