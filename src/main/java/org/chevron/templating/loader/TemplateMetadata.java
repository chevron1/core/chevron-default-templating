package org.chevron.templating.loader;

import java.io.Serializable;

public class TemplateMetadata implements Serializable {

    private String language;
    private String schema;

    TemplateMetadata(String language, String schema){
        this.language = language;
        this.schema = schema;
    }

    public String getLanguage() {
        return language;
    }

    public String getSchema() {
        return schema;
    }

}
